import csv
import pandas
def csv(_file):
    with open(_file, "r") as f:
        sniffer = csv.Sniffer()
        lines = f.readline()
        has_headers = sniffer.has_headers(lines)
    
    with open(_file, "r") as f:
        reader = csv.reader(f)
        headers = reader.readrow()
        rest_of = list(map(lambda l: l.readrow(), reader))
