import jsonlines
import os
import magic

mime = magic.Magic(mime=True)

def get_metadata(resource):
    id = resource["identifier"]
    resource_metadata = resource["metadata"]
    format = resource_metadata["format"]
    name = resource_metadata["name"]
    url = resource_metadata["url"]

    try:
        type = mime.from_file(os.path.join("data", "resources", id))
    except FileNotFoundError:
        #print("Missing file:", id)
        return None
    return {
            "id": id,
            "format": format,
            "name": name,
            "url": url,
            "mimetype": type
            }

def is_geo(metadata):
    format = metadata["format"].lower()
    return ("shp" in format or "shape" in format or "kml" in format or "kmz" in format)

def process_shapefile(resource):
    format = resource["metadata"]["format"].lower()
    if "shp" in format or "shape" in format:
        try:
            id = resource["identifier"]
            type = mime.from_file(os.path.join("data", "resources", id))
            print(type, format, id)
            if type == "text/html":
                return True
            elif type == "application/zip":
                return True
            else:
                return False
        except FileNotFoundError:
            return False
    else:
        return False


def main():
    with jsonlines.open("data/validation/data.vic.gov.au/resource_list.jsonl", "r") as reader:
        resources = reader.read()

    #metadata = [get_metadata(resource) for resource in resources]
    #metadata = [e for e in metadata if e is not None]
    #mimetypes = set([e["mimetype"] for e in metadata])
    #print(mimetypes)
    shapefiles = [e for e in resources if process_shapefile(e)]
    print("Shapefiles:", len(shapefiles))

    #with jsonlines.open("data/validation/data.vic.gov.au/metadata_list_geo.jsonl", "w") as writer:
        #writer.write(geo)

main()
